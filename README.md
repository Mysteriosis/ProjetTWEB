Smart Lectures
==============

##Intro
TWEB project 1 is a javaScript application witch allow a teacher to upload pdf and give online lecture.
Students can join lectures and ask questions using an anonymous chat.

##Prerequisite
To use this project you need to have a active Amazon S3 account with an **userId** and an **userPassword**.  
Go to the [Amazon S3 website](http://aws.amazon.com/s3/) to sing up for a free S3 account if you do not have one.

##Installation
```
 # Copy from git
 git clone https://github.com/Iosis555/ProjetTWEB.git  
 cd ProjetTWEB  

 # Install dependencies
 sudo npm install  
 bower install --save

 # Launch MongoDB
 mongod
```
Before using the application, you have to enter your Amazon S3 credentials in both files **serve.sh** and **build.sh**.  
Replace keywords *yourUserId* and *yourUserPassword* with your personnal credentials, then you can use those scripts to launch or build the production version of the project:

```
 # Build the project in the /dest folder
 ./build.sh
 
 # Launch the application
 ./serve.sh
```

##Technical documentation
You can find some more documentation about the technologies we used in the ***/docs*** folder.

##Specification available
###Professor

Specification               | Détails                                       | Done ?
--------------------------- | :-------------------------------------------- | :----:
Register & Login            | Multiple checks client & server side (routes) | YES
Prepare lecture             | Uploads uses Amazon S3.                       | YES
Link PDF slides             | Ok.                                           | YES
Give lecture                | Ok.                                           | YES
Control slides              | Ok.                                           | YES
See feedback                | Ok.                                           | YES
Submit poll                 | Ok.                                           | YES
Explore archive             | All data are stored, we just have to create a view for it. | READY
See list of past lectures   | No view created. Data ready.                  | READY
Access a lecture transcript | No view created. Data ready.                  | READY


###Student

Specification               | Détails                                       | Done ?
--------------------------- | :-------------------------------------------- | :----:
Register & Login            | Multiple checks client & server side (routes) | YES
Explore archives            | "Archive" are just inactive lectures          | YES
See list of past lectures   | No view created. Data ready.                  | READY
Access a lecture transcript | No view created. Data ready.                  | READYJoin & attend lecture       | Ok.                                           | YESGive feedback               | Ok.                                           | YES
Asks questions              | Can use right chat to do it                   | YES
Answer poll                 | Read the right chat ?                         | YES

###AdminSpecification               | Détails                                       | Done ?
--------------------------- | :---------------------------------------------| :----:Monitor system              |  | NOConsult admin dashboard     |  | NO

#Heroku

[https://projectweb.herokuapp.com/](https://projectweb.herokuapp.com/)