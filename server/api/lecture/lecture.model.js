'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LectureSchema = new Schema({
  name: String,
  description: String,
  created_at: {type: Date, default: Date.now},
  _presentation: {type: Schema.Types.ObjectId, ref: 'File'},
  _readings: [{type: Schema.Types.ObjectId, ref: 'Reading'}],
  _author: {type: Schema.Types.ObjectId, ref: 'User'}
});

module.exports = mongoose.model('Lecture', LectureSchema);