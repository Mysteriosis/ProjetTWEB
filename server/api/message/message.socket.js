/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Message = require('./message.model');

exports.register = function(socket) {
  Message.schema.post('save', function (doc) {
    Message.findOne(doc)
    .populate({path:'_author', select:'name'})
    .exec(function(err, item) {
      onSave(socket, item);
    });
  });

  Message.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('message-' + doc._reading + ':save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('message-' + doc._reading + ':remove', doc);
}