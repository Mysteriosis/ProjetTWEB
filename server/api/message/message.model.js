'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MessageSchema = new Schema({
  _author: {type: Schema.Types.ObjectId, ref: 'User'},
  created_at: {type: Date, default: Date.now},
  text: String,
  type: Number,
  _reading: {type: Schema.Types.ObjectId, ref: 'Reading'}
});

module.exports = mongoose.model('Message', MessageSchema);