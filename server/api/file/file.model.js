'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var FileSchema = new Schema({
  name: String,
  path: String,
  type: String
});

module.exports = mongoose.model('File', FileSchema);