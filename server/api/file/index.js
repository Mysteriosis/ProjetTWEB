'use strict';

var express = require('express');
var controller = require('./file.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.hasRole('prof'), controller.create);
router.put('/:id', auth.hasRole('prof'), controller.update);
router.patch('/:id', auth.hasRole('prof'), controller.update);
router.delete('/:id', auth.hasRole('prof'), controller.destroy);

module.exports = router;