'use strict';

var _ = require('lodash');
var File = require('./file.model');
// var uuid = require('node-uuid');
// var multiparty = require('multiparty');
// var fs = require('fs');

// var postFile = function(req, res, format) {
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) {
//         var file = files.file[0];
//         var fileName = uuid.v4() + file.originalFilename.split('.').pop();
//         var contentType = file.headers['content-type'];
//         var tmpPath = file.path;
//         var destPath = __dirname + '/../../uploads/' + fileName + file;

//         if (format != undefined && format.indexOf(contentType) == -1) {
//             fs.unlink(tmpPath);
//             return res.status(400).send('Wrong file type.');
//         }

//         var is = fs.createReadStream(tmpPath);
//         var os = fs.createWriteStream(destPath);
//         is.pipe(os);
//         is.on('end', function() {
//           fs.unlinkSync(tmpPath);
//         })
//     });
//     return '/uploads/' + fileName;
// };


// Get list of files
exports.index = function(req, res) {
  File.find(function (err, files) {
    if(err) { return handleError(res, err); }
    return res.json(200, files);
  });
};

// Get a single file
exports.show = function(req, res) {
  File.findById(req.params.id, function (err, file) {
    if(err) { return handleError(res, err); }
    if(!file) { return res.send(404); }
    return res.json(file);
  });
};

// Creates a new file in the DB.
exports.create = function(req, res) {
  File.create(req.body, function(err, file) {
    if(err) { return handleError(res, err); }
    return res.json(201, file);
  });
};

// Updates an existing file in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  File.findById(req.params.id, function (err, file) {
    if (err) { return handleError(res, err); }
    if(!file) { return res.send(404); }
    var updated = _.merge(file, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, file);
    });
  });
};

// Deletes a file from the DB.
exports.destroy = function(req, res) {
  File.findById(req.params.id, function (err, file) {
    if(err) { return handleError(res, err); }
    if(!file) { return res.send(404); }
    file.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}