/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Reading = require('./reading.model');

exports.register = function(socket) {
  Reading.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Reading.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('reading-' + doc._id + ':save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('reading-' + doc._id + ':remove', doc);
}