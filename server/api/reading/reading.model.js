'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ReadingSchema = new Schema({
  code: String,
  page: Number,
  done: {type: Boolean, default: false},
  _lecture: {type: Schema.Types.ObjectId, ref: 'Lecture'},
  _reader: {type: Schema.Types.ObjectId, ref: 'User'}
});

module.exports = mongoose.model('Reading', ReadingSchema);