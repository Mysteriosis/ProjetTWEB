'use strict';

var _ = require('lodash');
var Reading = require('./reading.model');

var makeCode = function(numberOfChar) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < numberOfChar; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

// Get list of readings
exports.index = function(req, res) {
  Reading.find(req.query)
  .where('done', false)
  .exec(function (err, lectures) {
    if(err) { return handleError(res, err); }
    return res.json(200, lectures);
  });
};

// Get a single reading
exports.show = function(req, res) {
  Reading.findById(req.params.id, function (err, reading) {
    if(err) { return handleError(res, err); }
    if(!reading) { return res.send(404); }
    return res.json(reading);
  });
};

// Creates a new reading in the DB.
exports.create = function(req, res) {
  var genCode = makeCode(4);
  //TO-DO: check if code already exist in active readings

  var reading = {
    code: genCode,
    page: req.body.page,
    _lecture: req.body.lecture,
    _reader: req.body.prof
  }

  Reading.create(reading, function(err, reading) {
    if(err) { return handleError(res, err); }
    return res.json(201, reading);
  });
};

// Updates an existing reading in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Reading.findById(req.params.id, function (err, reading) {
    if (err) { return handleError(res, err); }
    if(!reading) { return res.send(404); }
    var updated = _.merge(reading, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, reading);
    });
  });
};

// Deletes a reading from the DB.
exports.destroy = function(req, res) {
  Reading.findById(req.params.id, function (err, reading) {
    if(err) { return handleError(res, err); }
    if(!reading) { return res.send(404); }
    reading.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}