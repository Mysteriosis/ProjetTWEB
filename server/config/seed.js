/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var mongoose = require('mongoose');
var User = require('../api/user/user.model');
var File = require('../api/file/file.model');
var Lecture = require('../api/lecture/lecture.model');
var Message = require('../api/message/message.model');

var userId1 = mongoose.Types.ObjectId();
var userId2 = mongoose.Types.ObjectId();
var fileId = mongoose.Types.ObjectId();
var lectureId = mongoose.Types.ObjectId();

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Student 1',
    email: 's',
    password: 's'
  }, {
    provider: 'local',
    _id: userId1,
    name: 's2',
    email: 'student2@student.com',
    password: 's2'
  }, {
    provider: 'local',
    _id: userId2,
    name: 'Prof 1',
    email: 'p',
    role: 'prof',
    password: 'p'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin'
  }, function() {
      console.log('finished populating users');
    }
  );
});

File.find({}).remove(function() {
  File.create({
    _id: fileId,
    name: 'persistence.pdf',
    path: 'https://s3.amazonaws.com/projetwebus/persistence.pdf',
    type: 'application/pdf'
  }, function() {
      console.log('finished populating files');
    }
  );
});

Lecture.find({}).remove(function() {
  Lecture.create({
    _id: lectureId,
    name: "Persistence of data",
    description: "Explain how to use MongoDB",
    _presentation: fileId,
    _author: userId2
  }, function() {
      console.log('finished populating lectures');
    }
  );
});