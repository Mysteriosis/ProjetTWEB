#Smart Lectures: Technical Documentation
===

#Index

- [Introduction](#introduction)
- [Technologies used](#technologies-used)
  - [Frameworks](#frameworks)
    - [Yeoman](#yeoman)
    - [AngularJS](#angularjs)
    - [Bootstrap](#bootstrap)
    - [Express.js](#express.js)
    - [MongoDB](#mongodb)
    - [Angular-Fullstack](#angular-fullstack)
  - [Javascript Applications](#javascript-applications)
    - [Node.js](#node.js)
    - [Bower](#bower)
    - [Socket.io](#socket.io)
    - [Grunt](#grunt)
    - [Mongoose](#mongoose)
  - [External services](#external-services)
    - [Github](#github)
    - [Heroku](#heroku)
    - [Amazon S3](#amazon-s3)
- [How works the lecture ?](#how-works-the-lecture-)
  - [Explanation](#explanation)
  - [Sequence diagram](#sequence-diagram)
- [Issues encountered](#issues-encountered)
  - [Amazon S3 credentials](#amazon-s3-credentials)
  - [Amazon S3 credentials storage](#amazon-s3-credentials-storage)

#Introduction
If you're enjoyed by our concept and this first test application, you can freely fork and complete this project with you ideas or needs. This document aims to help developers to understand the tools we used and how we used them to reach our goal.

Our project is a web application. It's mean that, like all web-relative work, you need to have a server-client relationship between a front and a back-end. To help us in this task, we used a scaffolding tool called [Yeoman](http://yeoman.io/) to help us. But Yeoman is not really smart, you have to tell it what to do, and for this point, we used [DaftMonk](https://github.com/DaftMonk/)'s generator [Angular-fullstack](https://github.com/DaftMonk/generator-angular-fullstack) who gives Yeoman the plans to build the bases of our application.

Now that the foundations of our project are build, we (you) can scaffold new functionalities.

***Note***: Maybe my english is not perfect, but I think a bad english will still be more understandable for the rest of the world that a well-formed french. Merci !

#Technologies used
Our project uses many frameworks and applications to help us to devellops all its sides. You can see below general list of what we used to make the project and their purpose.

##Frameworks
###Yeoman
[Yeoman](http://yeoman.io/) is a scaffolding framework. Yeoman generate files / code for a specific task or for an entire website. Initially, Yeoman is deliver with a client-side generator but he can be configured to generate server-side code or even both.  
With the ``yo`` command, we can ask it to create the base of the application or a specific part, like an API ressource or a client-side view (page).  
You'll find some code sample below, in the [Angular-Fullstack](#angular-fullstack) generator description.

###AngularJS
[AngularJS](https://angularjs.org/) is an application framework maintained by Google.  
Angular create a *single-page application* that, as his name implies, deliver the content of the website in only one page (ligther, faster, so normally, better).  

We use Angular as a client-side front-end, it aim is to interract with the user by giving it the tools to communicate with the server and to generate the content sended by the server.  
We use the platform [PDF.js](http://mozilla.github.io/pdf.js/) to render for parsing and rendering PDFs in Angular views.

###Bootstrap
[Bootstrap](http://getbootstrap.com/) is a free front-end framework containing a collection of tools for creating websites and web applications. It contains HTML and CSS-based design templates for typography, forms, buttons, navigation and other interface components, as well as optional JavaScript extensions.  

Added in Angular through the bower package "angular-bootstrap", it is set by default when you use Angular-Fullstack. We use it to page our application and to make it user-friendly and of course delightfully shiny.

###Express.js
[Express](http://expressjs.com/) is our back-end web framework for Node.js. Express allow us to easely create REST endpoints to communicate with Angular. It manage the data and the security and share informations between users throught websockets.  

As a server-size framework, Express exclusively works with REST API to deliver data to client-side application. So, theorically, you can re-develop a front-end with an other framework like [Backbone.js](http://backbonejs.org/) for exemple, and continue using our own express back-end.

###MongoDB
[MongoDB](http://www.mongodb.org/) is a document-oriented noSQL database. Mongo saves its data in BSON (binary JSON) format, that it fits very well with our developement environement. It allow no-schema writing (add columns *on-the-fly*) and concurrent access.  

We naturally use MongoDB to persist data.

###Angular-Fullstack
[Angular-fullstack](https://github.com/DaftMonk/generator-angular-fullstack) is the little speciality of our application. As we said in the introduction, Angular-Fullstack is the generator used with Yeoman, in other words, it's the construction plan gave to Yeoman to scaffold our project.  

As we told you before, Yeoman is deliver with a client-side generator, but Angular-Fullstack will allow us to scaffold the client-side, the server-side and event configure a developement pipeline with grunt.

So when you have to add something in the app, please consider the ``yo``commands options offers by the generator. For exemple, create an API new endpoint (a new ressource) can be easely perform with the command:

```
$ yo angular-fullstack:endpoint endpoint_name
```

Where *endpoint_name* is the name of your new ressource.

You can also find a tool to quickly upload the project to a hosting service like Heroku (read below). Simply follow this steps: build, upload and add a MongoDB service):

```
$ grunt buildcontrol:openshift
$ yo angular-fullstack:heroku
$ heroku addons:add mongolab
```
Please note that DaftMonk's Angular-fullstack repository propose to install heroku's add-on **mongohq** . This add-on cost $14.00 / month but our proposition, "**MongoLab**", is currently free of charges. 

##Javascript Applications
###Node.js
[Node.js](http://nodejs.org/) is a platform built on Chrome's JavaScript runtime for easily building fast, scalable network applications. Node is like a application server, you can develop a web server or a prompt application for your any operating system, all in Javascript.

Node is delivered with a package management system called [NPM](https://www.npmjs.com/) (Node Package Manager) that can be used to extends Node's functionalities. When you use Angular-fullstack, lots of packages are automatically installed when you create your application. But you can easely add others modules with ```npm install module_name```.  
Take a look at the NPM website to see how much packages are available.

###Bower
[Bower](http://bower.io/) is a package management system for client-side programming. Actually, Bower do exactly the same work as NPM, but for our front-end application (AngularJS). Bower can add and manage dependencies for a lot of plugins, add-ons and applications.  
For exemple, you can add a forms generator for Angular by using the ```bower install forms-angular --save```command. Bower will download and check dependencies (e.g. the version of AngularJS), the **--save** option is used to add your new package into the ***bower.json*** file who list all the dependencies of your application.

###Socket.io
[Socket.io](http://socket.io/) is a Node module that enables real-time bidirectional event-based communication. By using the websocket technology, you can now **push** data through HTTP for create user-friendly, lags free and useful application like chats or schedule manager.  

```
io.on('connection', function(socket){
  socket.on('chat', function(msg){
    io.emit('chat', msg);
  });
});
```
Socket.io use the Node's http native module. He can trigger a HTTP event and do a specific action. On this example (server-side), socket.io broadcast every massages it receive from the client (by using the same ```io.emit('chat', msg)```) and all the others client connected to the websocket "chat" will receive the message.

Every server-side application that need to **talk** to its users when something happen needs it. Again, socket.io is a part of angular-fullstack, and can be added when you create a project.   

We use it for syncing the lecture file between teachers and students and of course, for the chat.

###Grunt
[Grunt](http://gruntjs.com/) is a task runner. And by task runner we say your best ally in this work. Grunt will perform a lot of repetitive tasks like minification, compilation, unit testing, linting, etc... Every tasks are performed in a pipeline logic, which means Grunt wont launch the app if he detect an error or if the unit testing task fails.  
When you add a new module with bower, use a ```grunt build```command, and grunt will link all this new stuff in your application, by adding HTML tag ```<script src="..."></script>```in your Angular template.  

Grunt is used when you want to test your application, this smart guy will even re-launch your application if it detect a modification in your source code. You can configure it with the **gruntfile.json** in the root directory.

###Mongoose
[Mongoose](http://mongoosejs.com/) is an ODM (Object Document Mapper). It's like an ORM (Object-Relational Mapping) but as we can't talk of a "relational" database with MongoDB, it's here called "document". And yes, it's also a Node.js module added by angular-fullstack.  

Mongoose is all we love when we have to deal with a database. It provides a straight-forward, schema-based solution to modeling your application data and includes built-in type casting, validation, query building, business logic hooks and more, out of the box.  

You can create a model to force the validation of your data (remember MongoDB can add columns *on-the-fly*).

```
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var petSchema = new Schema({
  name:  String,
  breed: String,
  owner: {type: Schema.Types.ObjectId, ref: 'Person'},
  birthday: {type: Date},
  isDead: Boolean
});
```

Even if there are no relations mechanisms, using a **Schema.Types.ObjectId** type can say to mongoose "perform a *find()* action to populate the **owner** key with the data of a particular **Person**).

##External services

###Github
[Github](https://github.com/) is a web-based Git repository hosting service, which offers all of the distributed revision control and source code management functionality of [Git](http://git-scm.com/).  

We use GitHub to store our project's source code. But you must know that if you read these lines... :-)

###Heroku
[Heroku](https://www.heroku.com/home) is a cloud platform as a service (PaaS) supporting several programming languages. Heroku can run our application free of charges and offer an URL to access it. Even if it seems tricky to upload the project, follow carefully the documentation of angular-fullstack to reach your goal.  

We use Heroku to host our [demo](projectweb.herokuapp.com) app.

###Amazon S3
[Amazon S3](http://aws.amazon.com/fr/s3/) (Simple Storage Service) is a storage service that provides developers and IT teams with secure, durable, highly-scalable object storage. You can store every kind of files on S3 but it's more logical to upload only small objects, like images or PDFs, it's better if it's fast.  

We use Amazon S3 to store all the PDF used by the teachers, so we don't have to manage the upload (S3 provides a JS SDK), we only store the generated URL.

#How works the lecture ?
##Explanation
Firstly, when a student read a lecture, he can freely turn the pages of the PDF, but the chat inactive. If a teacher starts a lecture near him, a reading code appears on the teacher screen. This code is an unique ID for this specific lecture.

Secondly, the student can enter the code and start to follow the lecture. From there, the PDF will be synced with the teacher's, and the pages will be turned at the same time. The chat is now available. Only students can write into it, the teacher can only read the texts ans the mood alerts. This is his responsability to interact with the students directly in case of question or bad reading.

If the student decide to change manually the page, he will lost the syncing and he'll have to enter the code again. If he do it, he will be lead to the actual page readed by the teacher and the chat will be updated.

##Sequence diagram
As you can see below, the turning pages mechanism
![](https://raw.githubusercontent.com/Iosis555/ProjetTWEB/master/docs/sequence_tweb.png "Sequence diagram")

#Issues encountered

##Amazon S3 credentials
We had a problem when we tried to use the Amazon S3 service. We wanted to use the Frankfurt's server but it desperately refuse our well-formed credentials because of some invalid HTTP headers. Even if we think it's due to the service's fourth version, we solved the problem by switching on Dublin's server. With no changes in our code, the service started to work correctly.  
So all we can say is to avoid the Frankfurt's server and, more generally, all the servers that use the fourth version of S3.

##Amazon S3 credentials storage
Actually, we store our credentials directly into our source code. That's wrong and Amazon do not like it. We know that's bad practice and we will change it as soon as possible.  
Do not make the same mistake and follow the next solution instead: [http://stackoverflow.com/questions/27904489/using-aws-credentials-in-a-public-github-repo-in-a-node-js-project-built-with](http://stackoverflow.com/questions/27904489/using-aws-credentials-in-a-public-github-repo-in-a-node-js-project-built-with)  
**Update**: Upon the Amazon's insistence, we fix this with the solution above.
