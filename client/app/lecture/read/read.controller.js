'use strict';

angular.module('projectwebApp')
  .controller('ReadCtrl', function ($scope, $http, socket, Auth, $window, $stateParams) {
    var user = Auth.getCurrentUser();
    var chatSpace = 230;

    PDFJS.workerSrc = 'bower_components/pdfjs-dist/build/pdf.worker.js';

    $scope.messages = [];
    $scope.readings = [];

    $scope.lecture = null;
    $scope.url = null;
    $scope.page = 1;
    $scope.code = null;

    $scope.following = false;

    //Check Roles
    if(user.role === "prof"){
        $scope.isProf = $scope.isProfInReading = true;
        chatSpace = 150;

        $scope.startNewReading = function() {
            $http.post('/api/readings/', {
                prof: user._id,
                lecture: $scope.lecture._id,
                page: $scope.page
            }).success(function(data, status, headers, config) {
                $scope.code = data.code;
                $scope.readings.push(data);
                socket.syncUpdates('message-' + data._id, $scope.messages);
                //socket.syncUpdates('reading-' + data._id, $scope.readings);
            });
        };
    }
    else {
        $scope.isProf = false;
    }

    //SubNavBar
    var show = true;

    $scope.isShow = function() {
        return show;
    }

    $scope.toggleShow = function() {
        show = (show)?false:true;
    }

    //JQuery Events
    jQuery(window).resize(function(event){
        $scope.$apply(function(){
            jQuery('#chat').height(jQuery(window).height()-chatSpace);
        });
    });

    //PDF JS
    var pdfDoc = null,
        pageRendering = false,
        pageNumPending = null,
        scale = 0.8,
        canvas = document.getElementById('slides'),
        ctx = canvas.getContext('2d');

    /**
     * Get page info from document, resize canvas accordingly, and render page.
     * @param num Page number.
     */
    function renderPage(num) {
        pageRendering = true;

        // Using promise to fetch the page
        pdfDoc.getPage(num).then(function(page) {
            var viewport = page.getViewport(scale);
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            jQuery('#chat').height(jQuery(window).height()-chatSpace);

            // Render PDF page into canvas context
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);

            // Wait for rendering to finish
            renderTask.promise.then(function () {
                pageRendering = false;
                if (pageNumPending !== null) {
                    // New page rendering is pending
                    renderPage(pageNumPending);
                    pageNumPending = null;
                }
            });
        });
    }

    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    function queueRenderPage(num) {
        if($scope.isProf)
            $scope.updateReading(num);

        if (pageRendering) {
            pageNumPending = num;
        } else {
            renderPage(num);
        }
    }

    /**
     * Displays previous page.
     */
    function onPrevPage() {
        if ($scope.page <= 1) {
            return;
        }

        //If button pushed, we stop following lecture
        if($scope.following && !$scope.isProf) {
            socket.unsyncUpdates('message-' + $scope.readings[0]._id, $scope.messages);
            $scope.following = false;
        }

        $scope.page--;
        queueRenderPage($scope.page);
    }
    document.getElementById('prev').addEventListener('click', onPrevPage);

    /**
     * Displays next page.
     */
    function onNextPage() {
        if ($scope.page >= pdfDoc.numPages) {
            return;
        }

        //If button pushed, we stop following lecture
        if($scope.following && !$scope.isProf) {
            socket.unsyncUpdates('message-' + $scope.readings[0]._id, $scope.messages);
            $scope.following = false;
        }
        
        $scope.page++;
        queueRenderPage($scope.page);
    }
    document.getElementById('next').addEventListener('click', onNextPage);

    $http.get('/api/lectures/' + $stateParams.id).success(function (lecture) {
        $scope.lecture = lecture;
        $scope.url = lecture._presentation.path;

        /**
         * Asynchronously downloads PDF.
         */
        PDFJS.getDocument($scope.url).then(function (pdfDoc_) {
            pdfDoc = pdfDoc_;
            renderPage($scope.page);
        });
    });

    //READINGS
    $scope.followReading = function() {
        var code = document.getElementById('codeInput').value;

        if($scope.following) {
            console.log($scope.following);
            socket.unsyncUpdates('message-' + $scope.readings[0]._id, $scope.messages);
            $scope.following = false;
        }
        else {
            $http.get('/api/readings/?_lecture=' + $stateParams.id + '&code=' + code)
                .success(function (readings) {
                    console.log(readings);
                    console.log($scope.following);

                    if(readings.length < 1) return;

                    document.getElementById('btn-input').value = '';

                    $scope.readings = readings;
                    var reading = readings[0];
                    $scope.page = reading.page;
                    queueRenderPage($scope.page);

                    socket.syncUpdates('message-' + $scope.readings[0]._id, $scope.messages, function(event, item, object) {
                        if(item.type == 2) {
                            var newPage = parseInt(item.text.split(' ')[4]);

                            console.log(item);
                            console.log(newPage);

                            if (newPage <= 1) return;
                            $scope.page = newPage;
                            queueRenderPage(newPage);
                        }
                    });
                    $scope.following = true;
                })
                .error(function() {
                    $scope.following = false;
                });
        }
    };

    $scope.updateReading = function(num) {
        $scope.page = num;

        if($scope.readings.length > 0) {
            $scope.readings[0].page = num;
            $http.put('/api/readings/' + $scope.readings[0]._id, $scope.readings[0]);
        }

        $scope.addMessage(2);
    }

    $scope.addMessage = function(msgType){

        var input = document.getElementById('btn-input');

        console.log($scope.readings);
        console.log($scope.following);

        switch(msgType) {
            case 1:
                if(!$scope.isProf) {
                    if(input.value === '' || input.value === undefined){return;}
                    $http.post('/api/messages',
                    {
                        _author: Auth.getCurrentUser()._id,
                        text: input.value,
                        type: msgType,
                        _reading: $scope.readings[0]._id
                    });
                    input.value = '';
                }
                break;
            case 2:
                if($scope.readings.length > 0) {
                    $http.post('/api/messages',
                    {
                        _author: Auth.getCurrentUser()._id,
                        text: '- Move to page ' + $scope.page + ' -',
                        type: msgType,
                        _reading: $scope.readings[0]._id
                    });
                }
                break;
            case 3:
                if($scope.readings.length > 0) {
                    $http.post('/api/messages',
                    {
                        _author: Auth.getCurrentUser()._id,
                        text: '[!] Too slow !',
                        type: msgType,
                        _reading: $scope.readings[0]._id
                    });
                }
                break;
            case 4:
                if($scope.readings.length > 0) {
                    $http.post('/api/messages',
                    {
                        _author: Auth.getCurrentUser()._id,
                        text: '[!] Too Fast !',
                        type: msgType,
                        _reading: $scope.readings[0]._id
                    });
                }
                break;
        }

        if(!$scope.isProf)
            input.value = 'Please join a reading to use the chat.';
    };
  });