'use strict';

angular.module('projectwebApp')
    .controller('CreateCtrl', function ($scope, $http, socket, Auth, $window, $location, $stateParams, ngProgress) {
    
    /* 
     * Source:
     
     * http://www.cheynewallace.com/uploading-to-s3-with-angularjs/
     * http://stackoverflow.com/questions/25946167/amazon-s3-upload-image-using-angular-js-directly-from-browser
     */
    $scope.creds = {
      bucket: 'projetwebus',
      access_key: '@@AMZN_USER_ID',
      secret_key: '@@AMZN_USER_PASSWORD'
    }

    $scope.upload = function() {
      
    }

    $scope.create = function(form) {
        
      if(form.$valid) {
        // Configure The S3 Object 
        var bucket = new AWS.S3({
          // region: 'eu-central-1',
          region: 'us-east-1',
          credentials: new AWS.Credentials($scope.creds.access_key, $scope.creds.secret_key)
        });
       
        if($scope.file) {
          var params = {
            Bucket: $scope.creds.bucket,
            Key: $scope.file.name,
            ContentType: $scope.file.type,
            Body: $scope.file,
            ServerSideEncryption: 'AES256'
          };

          bucket.putObject(params, function(err, data) {
            if(err) {
              // There Was An Error With Your S3 Config
              swal("An error occured", err.message, "error");
            }
            else {
              //Add Lecture
              ngProgress.complete();

              $http.post('api/lectures', {
                lecture: {
                  name: $scope.lecture.name,
                  description: $scope.lecture.description,
                  _author: Auth.getCurrentUser()._id
                },
                presentation: {
                  name: $scope.file.name,
                  path: 'https://s3.amazonaws.com/' + $scope.creds.bucket + '/' + $scope.file.name,
                  type: $scope.file.type
                }
              }).success(function(data, status, headers, config) {
                swal({
                  title: "New lecture created !",
                  text: "A new lecture has been created, you can now teaching a class with it !",
                  type: "success"
                }, function() { 
                  $location.path('/lecture');
                });
              }).error(function(err) {
                err = err.data;
                $scope.errors = {};

                // Update validity of form fields that match the mongoose errors
                angular.forEach(err.errors, function(error, field) {
                  form[field].$setValidity('mongoose', false);
                  $scope.errors[field] = error.message;
                });

                //TO-DO: Delete file on S3
              });
            }
          })
          .on('httpUploadProgress', function(progress, response) {
            // Progress Information
            ngProgress.set(Math.round(progress.loaded / progress.total * 100));
          });
        }
        else {
          swal("No File Selected", "You have to select a file !", "error");
        }
      }
    }
  }).directive('file', function() {
      return {
        restrict: 'AE',
        scope: {
          file: '@'
        },
        link: function(scope, el, attrs) {
          el.bind('change', function(event){
            var files = event.target.files;
            var file = files[0];
            scope.file = file;
            scope.$parent.file = file;
            scope.$apply();
          });
        }
      };
  });
