'use strict';

describe('Controller: CurrentlectureCtrl', function () {

  // load the controller's module
  beforeEach(module('projectwebApp'));

  var CurrentlectureCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CurrentlectureCtrl = $controller('CurrentlectureCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
