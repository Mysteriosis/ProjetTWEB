'use strict';

angular.module('projectwebApp')
    .controller('LectureCtrl', function ($scope, $http, socket, Auth, $window) {

        $scope.lectures = [];

        if(Auth.getCurrentUser().role === "prof"){
            $scope.isProf = true;
        }
        else {
            $scope.isProf = false;
        }

        $http.get('/api/lectures').success(function (lectures) {
            $scope.lectures = lectures;
        });

        $http.get('/api/lectures?orderBy=_readings&order=desc&limit=5').success(function (fav) {
            $scope.favorites = fav;
        });

        $http.get('/api/lectures?orderBy=created_at&order=desc&limit=5').success(function (recent) {
            $scope.newer = recent;
        });

    });
