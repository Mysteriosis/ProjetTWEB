'use strict';

describe('Controller: LectureCtrl', function () {

  // load the controller's module
  beforeEach(module('projectwebApp'));

  var LectureCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LectureCtrl = $controller('LectureCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
