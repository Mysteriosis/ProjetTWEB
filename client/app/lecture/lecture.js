'use strict';

angular.module('projectwebApp')
  .config(function ($stateProvider) {
    $stateProvider
        .state('lecture', {
        url: '/lecture',
        templateUrl: 'app/lecture/lecture.html',
        controller: 'LectureCtrl'
        })
        .state('read', {
        url: '/lecture/read/:id',
        templateUrl: 'app/lecture/read/read.html',
        controller: 'ReadCtrl'
        })
        .state('create', {
        url: '/lecture/create',
        templateUrl: 'app/lecture/create/create.html',
        controller: 'CreateCtrl'
        })
  });