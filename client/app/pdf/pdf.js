'use strict';

angular.module('projectwebApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('pdf', {
        url: '/pdf',
        templateUrl: 'app/pdf/pdf.html',
        controller: 'PdfCtrl'
      });
  });